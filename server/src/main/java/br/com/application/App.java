package br.com.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan
@SpringBootApplication
@EnableConfigurationProperties({LiquibaseProperties.class, Properties.class})
public class App {

    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }
}
