package br.com.application.rest;


import br.com.application.config.Constants;
import br.com.application.model.security.User;
import br.com.application.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URISyntaxException;
import java.util.List;

@RestController
@RequestMapping(Constants.URL)
public class UserResource {

    @Autowired
    private UserService service;

    public UserResource() {
    }

    @GetMapping("/user")
    @CrossOrigin(origins = "*")
    public List<User> getEntidades() {
        return service.findAll();
    }

    @PostMapping("/user")
    @CrossOrigin(origins = "*")
    public ResponseEntity<User> save(@Valid @RequestBody User entidade) throws URISyntaxException {
        User result = service.save(entidade);
        return ResponseEntity.status(HttpStatus.CREATED).body(result);
    }
}
