package br.com.application.rest;

import br.com.application.config.Constants;
import br.com.application.model.Andress;
import br.com.application.model.Student;
import br.com.application.service.AndressService;
import br.com.application.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URISyntaxException;
import java.util.List;

@RestController
@RequestMapping(Constants.URL)
public class StudentResource {

    @Autowired
    private StudentService service;
    @Autowired
    private AndressService andressService;

    public StudentResource() {
    }

    @GetMapping("/student")
    @CrossOrigin(origins = "*")
    public List<Student> getEntidades() {
        return service.findAll();
    }

    @PostMapping("/student")
    @CrossOrigin(origins = "*")
    @Transactional
    public ResponseEntity<Student> save(@Valid @RequestBody Student entidade) throws URISyntaxException {
        if (entidade.getAndress() != null) {
            System.out.println("entidade.getAndress() " + entidade.getAndress());
            entidade.setAndress(andressService.save(entidade.getAndress()));
        }
        Student result = service.save(entidade);
        return ResponseEntity.status(HttpStatus.CREATED).body(result);
    }
}
