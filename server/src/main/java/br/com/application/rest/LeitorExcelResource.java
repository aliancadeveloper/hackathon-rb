package br.com.application.rest;

import br.com.application.config.Constants;
import br.com.application.model.Andress;
import br.com.application.model.School;
import br.com.application.model.Serie;
import br.com.application.model.enums.TypeSchool;
import br.com.application.service.AndressService;
import br.com.application.service.SchoolService;
import br.com.application.service.SerieService;
import br.com.application.service.StudentService;
import br.com.application.util.Util;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationHome;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.net.URISyntaxException;

/**
 * Created by renatoromanini on 29/07/17.
 */

@RestController
@RequestMapping(Constants.URL_PUBLICA)
public class LeitorExcelResource {

    @Autowired
    private SerieService serieService;
    @Autowired
    private SchoolService schoolService;
    @Autowired
    private AndressService andressService;
    @Autowired
    private StudentService studentService;

    @GetMapping("/leitor-excel")
    @CrossOrigin(origins = "*")
    public ResponseEntity<String> comecarLeitura() throws URISyntaxException {
        String retorno = "veio aqui ";
        try {
            serieService.delete();
            schoolService.delete();
            studentService.delete();
            andressService.delete();

            ApplicationHome home = new ApplicationHome(this.getClass());
            File jarDir = home.getDir();
            String url = jarDir.getAbsolutePath() + "/server/src/main/resources/excel/schools_for_map.xlsx";
            File file = new File(url);

            retorno += file.getName();

            org.apache.poi.ss.usermodel.Workbook workbook = WorkbookFactory.create(file);
            org.apache.poi.ss.usermodel.Sheet sheet = workbook.getSheetAt(0);
            int rowsCount = sheet.getLastRowNum();

            for (int i = 1; i <= rowsCount; i++) {

                Row row = sheet.getRow(i);
                if (row != null) {
                    int colCounts = row.getLastCellNum();
                    String nome = "";
                    String telefone = "";
                    String coordenado = "";
                    String endereco = "";
                    String email = "";
                    String latitude = "";
                    String longitude = "";
                    String modalidade = "";
                    for (int j = 0; j < colCounts; j++) {
                        Cell cell = row.getCell(j);
                        String valorCell = Util.getValorCell(cell);


                        if (j == 0) {
                            nome = valorCell;
                        }
                        if (j == 1) {
                            telefone = valorCell;
                        }
                        if (j == 2) {
                            coordenado = valorCell;
                        }
                        if (j == 3) {
                            email = valorCell;
                        }
                        if (j == 4) {
                            endereco = valorCell;
                        }
                        if (j == 5) {
                            modalidade = valorCell;
                        }
                        if (j == 6) {
                            latitude = valorCell;
                        }
                        if (j == 7) {
                            longitude = valorCell;
                        }

                    }
                    if (!nome.trim().isEmpty() && !coordenado.trim().isEmpty() && !endereco.trim().isEmpty()) {
                        saveSchool(nome, telefone, coordenado, endereco, email, modalidade, longitude, latitude);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            retorno = " erroooor" + e.getMessage();
        }

        return ResponseEntity.status(HttpStatus.CREATED).body(retorno);
    }

    private void saveSchool(String nome, String telefone, String coordenado, String endereco, String email, String modalidade, String longitude, String latitude) {
        School school = new School();
        school.setName(nome);
        school.setDirector(coordenado);
        school.setEmail(email);
        if (!telefone.trim().isEmpty()) {
            school.setPhone(telefone);
        }
        for (TypeSchool typeSchool : TypeSchool.values()) {
            if (typeSchool.getDescription().equals(modalidade)) {
                school.setTypeSchool(typeSchool);
            }
        }
        if (school.getTypeSchool() == null) {
            throw new RuntimeException("Nenhum modadelida para a escola " + nome);
        }

        Andress andress = new Andress();
        andress.setAndress(endereco);
        andress.setLatitude(latitude);
        andress.setLongitude(longitude);
        andress = andressService.save(andress);

        school.setAndress(andress);

        school = schoolService.save(school);

        saveSeries(school);
    }

    private void saveSeries(School school) {
        int ensinoFundamental = 9;
        int ensinoMedio = 3;
        if (school.getTypeSchool().equals(TypeSchool.CRECHE)
                || school.getTypeSchool().equals(TypeSchool.PRE_ESCOLA)) {
            Serie serie = new Serie();
            serie.setName("1 ª Série");
            serie.setSchool(school);
            Serie save = serieService.save(serie);
        } else if (school.getTypeSchool().equals(TypeSchool.ENSINO_FUNDAMENTAL)) {
            for (int i = 1; i <= ensinoFundamental; i++) {
                Serie serie = new Serie();
                serie.setName(i + " ª Série");
                serie.setSchool(school);
                Serie save = serieService.save(serie);
            }
        } else if (school.getTypeSchool().equals(TypeSchool.ENSINO_MEDIO)) {
            for (int i = 1; i <= ensinoMedio; i++) {
                Serie serie = new Serie();
                serie.setName(i + " ª Série");
                serie.setSchool(school);
                Serie save = serieService.save(serie);
            }
        }

    }

}
