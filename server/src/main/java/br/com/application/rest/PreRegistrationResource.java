package br.com.application.rest;

import br.com.application.config.Constants;
import br.com.application.model.Andress;
import br.com.application.model.PreRegistration;
import br.com.application.model.Serie;
import br.com.application.model.Student;
import br.com.application.model.dto.StudentDTO;
import br.com.application.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(Constants.URL)
public class PreRegistrationResource {

    @Autowired
    private PreRegistrationService service;
    @Autowired
    private AndressService andressService;
    @Autowired
    private StudentService studentService;
    @Autowired
    private SerieService serieService;
    @Autowired
    private UserService userService;

    public PreRegistrationResource() {
    }

    @GetMapping("/preregistration")
    @CrossOrigin(origins = "*")
    public List<PreRegistration> getEntidades() {
        return service.findAll();
    }

    @PostMapping("/preregistration")
    @CrossOrigin(origins = "*")
    public ResponseEntity<PreRegistration> save(@Valid @RequestBody StudentDTO dto) throws URISyntaxException {
        Andress andress = new Andress();
        andress.setAndress(dto.getAddress());
        andress = andressService.save(andress);
        Student student = new Student();
        student.setAndress(andress);
        student.setDateBirth(dto.getDateBirth());
        student.setName(dto.getName());
        student.setTypeSexy(dto.getTypeSexy());
        student = studentService.save(student);
        PreRegistration preRegistration = new PreRegistration();
        preRegistration.setDate(new Date());
        preRegistration.setSerie(serieService.getOne(dto.getSerie()));
        preRegistration.setStudent(student);
        preRegistration.setUser(userService.getByLogin("admin"));
        preRegistration = service.save(preRegistration);
        Serie serie = preRegistration.getSerie();
        if (serie.getVacancy() > 0) {
            serie.setVacancy(serie.getVacancy() - 1);
            serie = serieService.save(serie);
        }
        return ResponseEntity.status(HttpStatus.CREATED).body(preRegistration);
    }
}
