package br.com.application.rest;

import br.com.application.config.Constants;
import br.com.application.model.School;
import br.com.application.model.Serie;
import br.com.application.model.dto.LatLngDTO;
import br.com.application.service.SchoolService;
import br.com.application.service.SerieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import static org.apache.poi.hslf.record.RecordTypes.List;

@RestController
@RequestMapping(Constants.URL)
public class SchoolResource {

    @Autowired
    private SchoolService service;
    @Autowired
    private SerieService serieService;

    public SchoolResource() {
    }

    @GetMapping("/school")
    @CrossOrigin(origins = "*")
    public List<School> getEntidades() {
        List<School> retorno = service.findAll();
        for (School school : retorno) {
            List<Serie> series = serieService.findAllBySchool(school);
            for (Serie serie : series) {
                school.setVacancy(school.getVacancy() + serie.getVacancy());
            }
        }
        return retorno;
    }

    @GetMapping(value = "/schoolbyname/",params = {"name"})
    @CrossOrigin(origins = "*")
    public ResponseEntity<List<School>> getEntidadesByName(@RequestParam(value = "name") String name) {
        java.util.List<School> result = service.findAllByName(name);
        return ResponseEntity.status(HttpStatus.CREATED).body(result);
    }

    @GetMapping("/coordenadas")
    @CrossOrigin(origins = "*")
    public List<LatLngDTO> getCoordenadas() {
        List<LatLngDTO> retorno = new ArrayList<>();
        List<School> all = service.findAll();
        for (School school : all) {
            if (school.getAndress().getLongitude() != null && school.getAndress().getLatitude() != null) {
                retorno.add(new LatLngDTO(school.getAndress().getLatitude(), school.getAndress().getLongitude()));
            }
        }
        return retorno;
    }

    @PostMapping("/school")
    @CrossOrigin(origins = "*")
    public ResponseEntity<School> save(@Valid @RequestBody School entidade) throws URISyntaxException {
        School result = service.save(entidade);
        return ResponseEntity.status(HttpStatus.CREATED).body(result);
    }
}
