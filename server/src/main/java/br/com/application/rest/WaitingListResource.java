package br.com.application.rest;

import br.com.application.config.Constants;
import br.com.application.model.WaitingList;
import br.com.application.service.WaitingListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URISyntaxException;
import java.util.List;

@RestController
@RequestMapping(Constants.URL)
public class WaitingListResource {

    @Autowired
    private WaitingListService service;

    public WaitingListResource() {
    }

    @GetMapping("/waitinglist")
    @CrossOrigin
    public List<WaitingList> getEntidades() {
        return service.findAll();
    }

    @PostMapping("/waitinglist")
    @CrossOrigin
    public ResponseEntity<WaitingList> save(@Valid @RequestBody WaitingList entidade) throws URISyntaxException {
        WaitingList result = service.save(entidade);
        return ResponseEntity.status(HttpStatus.CREATED).body(result);
    }
}
