package br.com.application.service;

import br.com.application.model.School;
import br.com.application.repository.SchoolRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;

/**
 * Created by renatoromanini on 27/07/17.
 */
@Service
public class SchoolService implements Serializable {

    private final Logger log = LoggerFactory.getLogger(SchoolService.class);
    @Autowired
    private SchoolRepository repository;

    @Transactional
    public School save(School obj) {
        log.debug("Request to save School : {}", obj);
        School result = repository.save(obj);
        return result;
    }

    @Transactional
    public School getOne(Long id) {
        log.debug("Request to save School : {}", id);
        School result = repository.getOne(id);
        return result;
    }

    @Transactional(readOnly = true)
    public List<School> findAll() {
        log.debug("Request to get all School");
        List<School> result = repository.findAll();
        return result;
    }

    @Transactional(readOnly = true)
    public List<School> findAllByName(String name) {
        System.out.println("Request to get all School by name " + name);
        List<School> result = repository.findFirtsByNameIgnoreCaseContaining(name);
        return result;
    }

    @Transactional
    public void delete() {
        log.debug("Request to delete School");
        repository.deleteAll();
    }

}
