package br.com.application.service;

import br.com.application.model.Andress;
import br.com.application.repository.AndressRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;

/**
 * Created by renatoromanini on 27/07/17.
 */
@Service
public class AndressService implements Serializable {

    private final Logger log = LoggerFactory.getLogger(AndressService.class);
    @Autowired
    private AndressRepository repository;

    @Transactional
    public Andress save(Andress obj) {
        log.debug("Request to save Andress : {}", obj);
        Andress result = repository.save(obj);
        return result;
    }

    @Transactional(readOnly = true)
    public List<Andress> findAll() {
        log.debug("Request to get all Andress");
        List<Andress> result = repository.findAll();
        return result;
    }

    @Transactional
    public void delete() {
        log.debug("Request to delete Andress");
        repository.deleteAll();
    }

}
