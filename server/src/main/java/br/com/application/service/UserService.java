package br.com.application.service;


import br.com.application.model.security.User;
import br.com.application.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;

/**
 * Created by renatoromanini on 27/07/17.
 */
@Service
public class UserService implements Serializable {

    private final Logger log = LoggerFactory.getLogger(UserService.class);
    @Autowired
    private UserRepository repository;

    @Transactional
    public User save(User obj) {
        log.debug("Request to save User : {}", obj.getPassword());
        User result = repository.save(obj);
        return result;
    }

    @Transactional
    public User getByLogin(String login) {
        log.debug("Request to get User by login: {}", login);
        User result = repository.findByLogin(login);
        return result;
    }

    @Transactional(readOnly = true)
    public List<User> findAll() {
        log.debug("Request to get all User");
        List<User> result = repository.findAll();
        return result;
    }

    @Transactional
    public void delete() {
        log.debug("Request to delete User");
        repository.deleteAll();
    }

}
