package br.com.application.service;

import br.com.application.model.PreRegistration;
import br.com.application.repository.PreRegistrationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;

/**
 * Created by renatoromanini on 27/07/17.
 */
@Service
public class PreRegistrationService implements Serializable {

    private final Logger log = LoggerFactory.getLogger(PreRegistrationService.class);
    @Autowired
    private PreRegistrationRepository repository;

    @Transactional
    public PreRegistration save(PreRegistration obj) {
        log.debug("Request to save PreRegistration : {}", obj);
        PreRegistration result = repository.save(obj);
        return result;
    }

    @Transactional(readOnly = true)
    public List<PreRegistration> findAll() {
        log.debug("Request to get all PreRegistration");
        List<PreRegistration> result = repository.findAll();
        return result;
    }

    @Transactional
    public void delete() {
        log.debug("Request to delete PreRegistration");
        repository.deleteAll();
    }

}
