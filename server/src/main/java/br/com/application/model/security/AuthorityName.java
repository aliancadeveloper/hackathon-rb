package br.com.application.model.security;

public enum AuthorityName {
    ROLE_USER, ROLE_ADMIN
}
