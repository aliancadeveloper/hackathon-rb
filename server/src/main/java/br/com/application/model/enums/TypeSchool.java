package br.com.application.model.enums;

/**
 * Created by renatoromanini on 29/07/17.
 */
public enum TypeSchool {
    CRECHE("Creche"),
    PRE_ESCOLA("Pré Escola"),
    ENSINO_FUNDAMENTAL("Ensino Fundamental"),
    ENSINO_MEDIO("Ensino Médio");

    private String description;

    TypeSchool(String description) {
        this.description = description;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return description;
    }
}