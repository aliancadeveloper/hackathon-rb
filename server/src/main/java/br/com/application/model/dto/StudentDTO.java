package br.com.application.model.dto;

import br.com.application.model.enums.TypeSexy;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by renatoromanini on 30/07/17.
 */
public class StudentDTO implements Serializable {


    private String name;
    @Temporal(TemporalType.DATE)
    private Date dateBirth;
    private String address;
    private TypeSexy typeSexy;
    private Long serie;

    public StudentDTO() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDateBirth() {
        return dateBirth;
    }

    public void setDateBirth(Date dateBirth) {
        this.dateBirth = dateBirth;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public TypeSexy getTypeSexy() {
        return typeSexy;
    }

    public void setTypeSexy(TypeSexy typeSexy) {
        this.typeSexy = typeSexy;
    }

    public Long getSerie() {
        return serie;
    }

    public void setSerie(Long serie) {
        this.serie = serie;
    }
}
