package br.com.application.model.dto;

/**
 * Created by renatoromanini on 30/07/17.
 */
public class LatLngDTO {
    private String lat;
    private String lng;

    public LatLngDTO(String latitude, String longitude) {
        this.lat = latitude;
        this.lng = longitude;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }
}
