package br.com.application.model;

import br.com.application.model.enums.TypeSchool;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * Created by renatoromanini on 27/07/17.
 */
@Entity
@Table(name = "school")
public class School implements Serializable {

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Size(min = 1, max = 100)
    @Column(length = 100, unique = true, nullable = false)
    private String name;

    @NotNull
    @Size(min = 1, max = 100)
    @Column(length = 100)
    private String director;

    @ManyToOne
    @NotNull
    private Andress andress;

    @Size(min = 1, max = 100)
    @Column(length = 100)
    private String phone;

    @Size(min = 0, max = 100)
    @Column(length = 100)
    private String email;


    @Column(name = "typeschool")
    @Enumerated(EnumType.STRING)
    private TypeSchool typeSchool;

    @Transient
    @JsonSerialize
    @JsonDeserialize
    private Integer vacancy;


    public School() {
        vacancy = 0;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public Andress getAndress() {
        return andress;
    }

    public void setAndress(Andress andress) {
        this.andress = andress;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public TypeSchool getTypeSchool() {
        return typeSchool;
    }

    public void setTypeSchool(TypeSchool typeSchool) {
        this.typeSchool = typeSchool;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getVacancy() {
        return vacancy;
    }

    public void setVacancy(Integer vacancy) {
        this.vacancy = vacancy;
    }
}
