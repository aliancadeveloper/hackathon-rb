package br.com.application.model.enums;

/**
 * Created by renatoromanini on 29/07/17.
 */
public enum TypeSexy {
    MASCULINO("Masculino"),
    FEMININO("Feminino");

    private String description;

    TypeSexy(String description) {
        this.description = description;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
