package br.com.application.model;

import br.com.application.model.enums.TypeSexy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by renatoromanini on 29/07/17.
 */
@Entity
@Table(name = "student")
public class Student implements Serializable {

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Size(min = 1, max = 100)
    @Column(length = 100, unique = true, nullable = false)
    private String name;

    @NotNull
    @Column(name = "DATEBIRTH")
    @Temporal(TemporalType.DATE)
    private Date dateBirth;

    @ManyToOne
    private Andress andress;

    @Column(name = "typesexy")
    @Enumerated(EnumType.STRING)
    private TypeSexy typeSexy;

    public Student() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDateBirth() {
        return dateBirth;
    }

    public void setDateBirth(Date dateBirth) {
        this.dateBirth = dateBirth;
    }

    public Andress getAndress() {
        return andress;
    }

    public void setAndress(Andress andress) {
        this.andress = andress;
    }

    public TypeSexy getTypeSexy() {
        return typeSexy;
    }

    public void setTypeSexy(TypeSexy typeSexy) {
        this.typeSexy = typeSexy;
    }
}
