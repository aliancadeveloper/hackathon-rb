package br.com.application.security;

import br.com.application.security.util.SecurityUtils;
import org.springframework.data.domain.AuditorAware;
import org.springframework.stereotype.Component;

/**
 * Created by zaca.
 */
@Component
public class SpringSecurityAuditorAware implements AuditorAware<String>{

    @Override
    public String getCurrentAuditor() {
        String userName = SecurityUtils.getCurrentUserLogin();
        return userName != null ? userName : "system";
    }
}
