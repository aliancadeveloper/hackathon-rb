package br.com.application.repository;

import br.com.application.model.PreRegistration;
import br.com.application.model.School;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by renatoromanini on 27/07/17.
 */
public interface PreRegistrationRepository extends JpaRepository<PreRegistration, Long> {

}
