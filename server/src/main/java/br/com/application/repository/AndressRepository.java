package br.com.application.repository;

import br.com.application.model.Andress;
import br.com.application.model.School;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by renatoromanini on 29/07/17.
 */
public interface AndressRepository extends JpaRepository<Andress, Long> {
}
