package br.com.application.repository;

import br.com.application.model.School;
import br.com.application.model.Serie;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by renatoromanini on 27/07/17.
 */
public interface SerieRepository extends JpaRepository<Serie, Long> {

    List<Serie> findFirtsBySchool(School school);
}
