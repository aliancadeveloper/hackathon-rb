package br.com.application;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Created by zaca on 26/07/17.
 */
@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
public class Properties {
}
