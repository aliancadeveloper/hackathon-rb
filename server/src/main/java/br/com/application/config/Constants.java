package br.com.application.config;

/**
 * Application constants.
 */
public final class Constants {

    //Regex for acceptable logins
    public static final String LOGIN_REGEX = "^[_'.@A-Za-z0-9-]*$";
    public static final String URL_PUBLICA = "/auth";
    public static final String URL = "/auth";

    private Constants() {
    }
}
