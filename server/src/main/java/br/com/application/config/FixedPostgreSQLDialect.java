package br.com.application.config;

import org.hibernate.dialect.PostgreSQL82Dialect;
import org.hibernate.type.descriptor.sql.BinaryTypeDescriptor;
import org.hibernate.type.descriptor.sql.SqlTypeDescriptor;

/**
 * Created by zaca.
 */
public class FixedPostgreSQLDialect extends PostgreSQL82Dialect {
    public FixedPostgreSQLDialect() {
        this.registerColumnType(2004, "bytea");
    }

    public SqlTypeDescriptor remapSqlTypeDescriptor(SqlTypeDescriptor sqlTypeDescriptor) {
        return (SqlTypeDescriptor)(sqlTypeDescriptor.getSqlType() == 2004? BinaryTypeDescriptor.INSTANCE:super.remapSqlTypeDescriptor(sqlTypeDescriptor));
    }
}
