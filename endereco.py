import urllib.parse
import requests
import json

buscar_escolas = requests.get("http://educacao.dadosabertosbr.com/api/escolas/buscaavancada?situacaoFuncionamento=1&estado=AC&cidade=1200401&dependenciaAdministrativa=3")

escolas = json.loads(buscar_escolas.content.decode('utf-8'))

lista_de_escolas = []

for x in escolas[1]:
    lista_de_escolas.append(x['nome'])


main_api = 'http://maps.googleapis.com/maps/api/geocode/json?'

for x in lista_de_escolas:
    url = main_api + urllib.parse.urlencode({'address': 'Acre Rio Branco'+x})
    json_data = requests.get(url).json()
    if json_data['results']:
        lat = json_data['results'][0]['geometry']['location']['lat']
        lng = json_data['results'][0]['geometry']['location']['lng']
        print(lat, lng)
