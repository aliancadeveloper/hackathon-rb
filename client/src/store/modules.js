import { store as app } from '../domain'

export default { ...app.modules }
