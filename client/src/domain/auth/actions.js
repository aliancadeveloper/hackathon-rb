import { postLogin, getVerifyToken, getRefreshToken } from 'services/auth'
import { isEmpty } from 'lodash'
import * as TYPES from './mutations-types'
import { userToken } from 'src/support'
import localforage from 'localforage'

export const doLogin = ({ dispatch }, user) => {
  return postLogin(user.username, user.password)
  .then(({ token, user }) => {
    dispatch('setUser', user)
    dispatch('setToken', token)
    return user
  }).catch(erro => Promise.reject(erro))
}

export const doLogout = ({ dispatch }) => {
  dispatch('setUser', {})
  dispatch('setToken', '')
  return Promise.resolve
}
export const setToken = ({ commit }, accessToken) => {
  const token = (isEmpty(accessToken)) ? null : accessToken || accessToken.token
  commit(TYPES.SET_TOKEN, token)
  return Promise.resolve(token)
}

export const setUser = ({ commit }, user) => {
  commit(TYPES.SET_USER, user)
  return Promise.resolve(user)
}

export const logout = context => {
  context.commit(TYPES.SET_TOKEN, null)
  context.commit(TYPES.SET_USER, {})
  context.commit(TYPES.SET_PERMISSION, [])
  return Promise.resolve
}

export const checkTokenVerify = ({ dispatch, commit }, token) => {
  return getVerifyToken(token)
  .then(({ token, user }) => {
    dispatch('setUser', user)
    dispatch('setToken', token)
    return token
  }).catch(erro => {
    return dispatch('refreshTokenValid', token)
  })
}

export const refreshTokenValid = ({ dispatch, state }, token) => {
  return getRefreshToken(token)
  .then(({ token, user }) => {
    dispatch('setUser', user)
    dispatch('setToken', token)
    return token
  }).catch(erro => {
    return Promise.reject(erro)
  })
}

export const checkTokenUser = ({ dispatch, state }) => {
  if (!isEmpty(state.token)) {
    return Promise.resolve(state.token)
  }

  return localforage.getItem(userToken)
    .then((token) => {
      if (isEmpty(token)) {
        return Promise.reject(userToken)
      }
      return dispatch('setToken', token) // keep promise chain
    }).catch(token => {
      return Promise.reject(token)
    })
}
