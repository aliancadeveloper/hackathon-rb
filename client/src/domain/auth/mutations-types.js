export const SET_TOKEN = 'auth/SET_TOKEN'
export const SET_USER = 'auth/SET_USER'
export const SET_PERMISSION = 'auth/SET_PERMISSION'
