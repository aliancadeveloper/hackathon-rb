import localforage from 'localforage'
import { userToken } from 'support'
import { setToken } from 'pluggables/http'
import * as TYPES from './mutations-types'

const subscribe = (store) => {
  store.subscribe((mutation, { auth }) => {
    if (TYPES.SET_TOKEN === mutation.type) {
      setToken(auth.token)
      localforage.setItem(userToken, auth.token)
    }
  })
}

export default (store) => {
  subscribe(store)
}
