import { http } from 'pluggables/http'

export const postUser = (user) => {
  return http.post('/auth/user', user)
  .then(response => response.data)
  .catch(err => Promise(err))
}
