import { http } from 'pluggables/http'

export const findAll = () => {
  return http.get('/auth/school')
  .then(response => response.data)
  .catch(() => Promise.reject)
}

export const find = (name) => {
  return http.get('/auth/schoolbyname/', {
    params: { name: name }
  })
  .then(response => response.data)
  .catch(err => Promise.reject(err))
}

export const findSeries = (id) => {
  return http.get('/auth/seriebyschool/', {
    params: {
      id: id
    }
  })
  .then(response => response.data)
  .catch(err => Promise.reject(err))
}
