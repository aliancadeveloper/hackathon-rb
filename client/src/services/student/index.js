import { http } from 'pluggables/http'

export const postStudent = (student) => {
  return http.post('/auth/student', student)
  .then(response => response.data)
  .catch(err => Promise.reject(err))
}
