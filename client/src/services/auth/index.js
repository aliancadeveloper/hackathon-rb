import { http } from 'pluggables/http'

export const postLogin = (username, password) => {
  return http.post('/auth', { username, password })
  .then(response => response.data)
  .catch(err => {
    console.log(' err ' + err)
    return Promise.reject(err)
  })
}

export const getVerifyToken = (token) => {
  return http.post('/auth/verify', { token })
  .then(response => response.data)
}

export const getRefreshToken = (token) => {
  return http.post('/auth/refresh', { token })
  .then(response => response.data)
}
