import { http } from 'pluggables/http'

export const postInscription = (preregistration) => {
  return http.post('/auth/preregistration', preregistration)
  .then(response => response.data)
}
