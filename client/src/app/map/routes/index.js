import Map from '../components/map'

export default [{
  name: 'map', path: '/map', component: Map, meta: { requiresAuth: false }
}]
