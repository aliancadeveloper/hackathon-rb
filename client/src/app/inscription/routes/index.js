import Inscription from '../components/inscription'

export default [{
  name: 'inscription', path: '/inscription', component: Inscription, meta: { requiresAuth: true }
}]
