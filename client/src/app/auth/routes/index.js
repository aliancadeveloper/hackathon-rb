import { Login } from './components'

export default [{
  name: 'auth', path: '/auth', component: Login, meta: { requiresAuth: false }
}]
