import School from '../components/school'

export default [{
  name: 'school', path: '/school', component: School, meta: { requiresAuth: true }
}]
