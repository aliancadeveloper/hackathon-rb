import { routes as auth } from './auth'
import { routes as school } from './school'
import { routes as inscription } from './inscription'
import { routes as user } from './user'
import { routes as student } from './student'
import Layout from 'components/root/layout'

import Home from 'components/root/home'

const routes = [
  {
    path: '/',
    component: Home,
    meta: { requiresAuth: true },
    children: [
    ]
  },
  {
    path: '',
    component: Layout,
    meta: { requiresAuth: true },
    children: [
      ...school,
      ...inscription,
      ...user,
      ...student
    ]
  },
  ...auth
]

export default [ ...routes ]
