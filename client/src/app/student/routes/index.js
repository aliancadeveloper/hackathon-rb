import Student from '../components/student'

export default [{
  name: 'student', path: '/student', component: Student, meta: { requiresAuth: true }
}]
