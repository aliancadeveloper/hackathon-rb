import User from '../components/user'

export default [{
  name: 'user', path: '/user', component: User, meta: { requiresAuth: true }
}]
