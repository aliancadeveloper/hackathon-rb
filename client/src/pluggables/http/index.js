import axios from 'axios'
import { api } from 'support'
import interceptor from './interceptor'

export const http = axios.create({
  baseURL: api
})

export function setToken (token) {
  http.defaults.headers['authentication-token'] = token
}

export default function install (Vue, { store, router }) {
  interceptor(http, store, router)
  Object.defineProperty(Vue.prototype, '$http', {
    get () {
      return http
    }
  })
}
