export default (http, store, router) => {
  http.interceptors.response.use(request => request, (error) => {
    const { response } = error

    if ([ 401, 400 ].indexOf(response.status) > -1) {
      // router.push({ name: 'auth' })
    }

    return Promise.reject(response)
  }
)
}
